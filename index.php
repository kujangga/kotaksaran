<?php
require ('functions.php');
$kotaksaran = query("SELECT * FROM kotaksaran");

//search data
if (isset ($_POST["cari"]) ){
    $kotaksaran = cari($_POST["keyword"]);
}

?>

<!DOCTYPE html>
<html>
<head>
</head>
    <title>Administrator</title>
    <body>
        <h1> Selamat Datang Admin </h1>
        <a href="index.php"> Home</a> |
        <a href="kotaksaran.php">Tambah Saran</a>
        <br>
        <br>
        <form action="" method="post">
            <input type="text" name="keyword" size="20" placeholder="Cari.." autocomplete="off">
            <button type="submit" name="cari"> Search </button>
        </form>
        <h2> Kotak Saran </h2>
        <table border="1" cellpadding="10" cellspacing="0">
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Tipe Pesan</th>
                <th>Pesan</th>
                <th>Aksi</th>
            </tr>
            <?php $i=1; ?>
            <?php foreach( $kotaksaran as $row ) : ?>
            <tr>
                <td> <?= $i; ?> </td>
                <td> <?= $row["nama"];?> </td>
                <td> <?= $row["tipepesan"];?> </td>
                <td> <?= $row["pesan"];?> </td>
                <td> <a href="edit.php?id=<?=$row["id"];?>"> Edit </a> |
                     <a href="delete.php?id=<?=$row["id"];?>" onclick="return confirm('dihapus?');"> Hapus </a> |
                     <a href=""> View </a>

            </tr>
            <?php $i++; ?>
            <?php endforeach; ?>
    </body>
</html>
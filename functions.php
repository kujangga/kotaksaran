<?php

//koneksi ke db
$conn = mysqli_connect("localhost", "root", "root", "belajar");

//ambil data di db
function query($query) {
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
        while ( $row = mysqli_fetch_assoc($result) ) {
            $rows[] = $row;
        }
        return $rows;
    }

//tambah data
function tambah($post) {
    global $conn;
    $nama     = htmlspecialchars($post["nama"]);
    $tipepesan= htmlspecialchars($post["tipepesan"]);
    $pesan    = htmlspecialchars($post["pesan"]);
    $query = " INSERT INTO kotaksaran VAlUES (0, '$nama', '$tipepesan', '$pesan' ) ";
    mysqli_query ($conn, $query);
    return mysqli_affected_rows($conn);
}

//hapus pesan
function hapus($id){
    global $conn;
    mysqli_query($conn, "DELETE FROM kotaksaran WHERE id = $id");
    return mysqli_affected_rows($conn);
}

//edit data
function edit($post){
    global $conn;
    $id       = $post["id"];
    $nama     = htmlspecialchars($post["nama"]);
    $tipepesan= htmlspecialchars($post["tipepesan"]);
    $pesan    = htmlspecialchars($post["pesan"]);
    $query = " UPDATE kotaksaran SET 
                nama = '$nama', 
                tipepesan = '$tipepesan', 
                pesan = '$pesan' 
                WHERE id = $id 
             ";
    mysqli_query ($conn, $query);
    return mysqli_affected_rows($conn);
}

//cari data
function cari($keyword){
    global $conn;
    $query = "SELECT * FROM kotaksaran 
                WHERE 
                nama LIKE '%$keyword%' OR
                tipepesan = '$keyword' OR
                pesan LIKE '%$keyword%'
             ";
    return query($query);

}

//registrasi
function register($post){
    global $conn;
    $username  = strtolower(stripslashes($post["username"]));
    $email     = strtolower(stripslashes($post["email"]));
    $password  = mysqli_real_escape_string($conn, $post["password"]);
    $password2 = mysqli_real_escape_string($conn, $post["password2"]);
    
    //cek username sama
    $result = mysqli_query($conn, "SELECT username FROM users WHERE
                    username = '$username'");
                    if (mysqli_fetch_assoc($result)){
                        echo "<script> alert('Username telah digunakan!') </script>";
                        return false;
                    }
    
    //konfirm password sama
    if ($password !== $password2) {
        echo " <script> 
                alert('Password tidak sama'); 
               </script> ";
        return false;
    }

    //encrypt password
    $password = password_hash($password, PASSWORD_DEFAULT);

    //add user in db
    mysqli_query($conn, "INSERT INTO users VAlUES (0, '$username', '$email', '$password')");
    return mysqli_affected_rows($conn);

}
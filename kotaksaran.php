<?php
require ('functions.php');
//ambildata
if( isset($_POST["submit"]) ){

  //cek data berhasil ditambah 
  if( tambah($_POST) > 0);{
    echo "<script> alert('Pesan berhasil di terima');
          document.location.href = 'index.php';
          </script>
          ";
  }
}
?>

<!DOCTYPE html>
  <head>
    <link href="css/style.css" rel="stylesheet">
    <title>Kotak Saran</title>
  </head>
  <body>
    <div class="container-fluid">
      <h3>Feedback Form</h3> 
      <form action="" method="post"> 

        <div class="row">
          <div class="col-25">        
            <label for="nama"> Nama </label>
          </div> 
            <div class="col-75">
              <input type="text" name="nama" id="nama" placeholder="Nama anda..." required>           
            </div>
        </div>

        <div class="row">
          <div class="col-25">
              <label for="tipepesan"> Tipe Pesan </label>
          </div>
            <div class="col-75">
              <select id="tipepesan" name="tipepesan">
                <option value="Saran"> Saran </option>
                <option value="Kritik"> Kritik </option>
              </select> 
            </div>
        </div>

        <div class="row">
          <div class="col-25">       
            <label for="pesan"> Pesan </label>
          </div>
            <div class="col-75">
              <textarea id="pesan" name="pesan" placeholder="Pesan..." style="height:55px" required></textarea>
            </div>
          </div>

        <div class="row">
            <button type="submit" name="submit"> Kirim </button>
        </div>

      </form>
    </div>
  </body>
</html>
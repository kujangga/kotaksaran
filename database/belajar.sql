-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 09, 2021 at 08:55 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `belajar`
--

-- --------------------------------------------------------

--
-- Table structure for table `kotaksaran`
--

CREATE TABLE `kotaksaran` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tipepesan` varchar(100) DEFAULT NULL,
  `pesan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kotaksaran`
--

INSERT INTO `kotaksaran` (`id`, `nama`, `tipepesan`, `pesan`) VALUES
(1, 'Bernard', 'Saran', 'boleh dong ditambahin cara buat export peserta biar ga insert manual di portal'),
(2, 'Salim', 'Kritik', 'tampilannya kurang cihuy'),
(8, 'kaka', 'Saran', 'Sweet as romance');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kotaksaran`
--
ALTER TABLE `kotaksaran`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kotaksaran`
--
ALTER TABLE `kotaksaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
